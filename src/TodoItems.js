import React, { Component } from "react";
import "./TodoList.css";
class TodoItems extends Component {
    constructor(props) {
        super(props);

        this.createTasks = this.createTasks.bind(this);
    
    }

    createTasks(item){
        return (
            <div>
                <li>
                    <li className={item.isCompleted ? 'todoCompleted' : null}  onClick={() => this.complete(item.key)} key={item.key}>{item.text}</li>
                    <button onClick={() => this.delete(item.key)} >Устгах</button>
                </li>
            </div>
        )
    }

    delete(key) {
        this.props.delete(key);
    }

    complete(key) {
        this.props.complete(key)
    }

    render() {
        var todoEntries = this.props.entries;
        var listItems = todoEntries.map(this.createTasks);

        return(
            <ul className="theList">
                {listItems}
            </ul>
        );
    }
}

export default TodoItems;