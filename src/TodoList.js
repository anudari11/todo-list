import React, { Component } from "react";
import TodoItems from "./TodoItems";
import "./TodoList.css";

class TodoList extends Component {
    constructor(props) {
        super(props);

        this.state = {
            items: []
        };

        this.addItem = this.addItem.bind(this);
        this.deleteItem = this.deleteItem.bind(this);
        this.completeItem = this.completeItem.bind(this);
    }

addItem(e) {
    if (this._inputElement.value !==""){
        var newItem = {
            text: this._inputElement.value,
            key: Date.now(),
            isCompleted: false
        };

        this.setState((prevState) => {
            return{
                items: prevState.items.concat(newItem)
            };
        });
    }

    this._inputElement.value="";
    console.log(this.state.items)

    e.preventDefault();
}

deleteItem(key) {
    const filteredItems = this.state.items.filter(item => {
        return item.key !== key
    })
    console.log(filteredItems)
    this.setState({
        items: filteredItems
    });
}

completeItem(key) {
    this.setState({
        items: this.state.items.map(item => {
            if (item.key === key) {
                item.isCompleted = !item.isCompleted
            }
            return item
        })
    })
}
render() {
    return (
        <div className="todoListMain">
            <div className="header">
                <form onSubmit={this.addItem}>
                    <input ref={(a) => this._inputElement = a} placeholder="Хийх зүйлээ бичнэ үү"></input>
                    <button type="submit">Нэмэх</button>
                </form>
            </div>
            <TodoItems entries={this.state.items} delete={this.deleteItem} complete={this.completeItem}/>
         </div>
  );
}
}

export default TodoList;

// NEW COMMITasdasd